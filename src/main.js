import Vue from 'vue'
import VueRouter from 'vue-router'
import vueResource from 'vue-resource'
import VueSession from 'vue-session'
import VueModal from 'vue-js-modal'

import app from './App.vue'
import {routes} from './route.js'

Vue.use(VueSession, {persist: true})
Vue.use(VueRouter)
Vue.use(vueResource)
Vue.use(VueModal)
Vue.http.options.root = 'https://apihobmeet.herokuapp.com/';

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
    el: "#App",
    router,
    components: {app},
    render: h => h(app)
});

