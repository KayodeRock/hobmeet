import landPage from './components/initial_page/Initial_frame.vue'
import homePage from './components/members/Home.vue'

export const routes = [
    {path:"", component: landPage},
    {path:"/login", component: landPage},
    {path:"/register", component: landPage},
    {path:"/dashboard/hobbies", component: homePage},
    {path:"/dashboard/profile", component: homePage}
]